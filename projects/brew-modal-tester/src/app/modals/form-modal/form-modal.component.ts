import {Component} from '@angular/core';
import {ModalActionBase} from 'projects/brew-modal-lib/src/lib/abstract/modal/modal-action-base';
import {GreeterAction} from '../../action/greeter-action';
import {CancelledGreeterAction} from '../../action/cancelled-greeter-action';

@Component({
    selector: 'app-form-modal',
    template: `
        <div class="modal-overlay background--transparent-black" *ngIf="show">
            <div class="modal-holder">
                <div *ngIf="title" class="modal-header modal--padding modal--section-divider">
                    <h2 class="modal-title">{{title}}</h2>
                </div>
                <div class="modal-body modal--padding modal--section-divider">
                    <ng-content></ng-content>
                    <app-form (formData)="handleFormSubmit($event)"></app-form>
                </div>
                <div class="modal-actions modal--padding">
                    <button (click)="handleCancellation()" class="button button--decline">Cancel</button>
                </div>
            </div>
        </div>
    `,
})
export class FormModalComponent extends ModalActionBase {
    handleFormSubmit(formData: any) {
        this.setConfirmAction(new GreeterAction(formData));
        this.confirmActionAndClose();
    }

    handleCancellation() {
        this.setCancelAction(new CancelledGreeterAction);
        this.cancelActionAndClose();
    }
}
