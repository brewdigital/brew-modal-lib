import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {FormModalComponent} from '../modals/form-modal/form-modal.component';
import {BrewMessageModalComponent} from 'projects/brew-modal-lib/src/lib/modal/message-modal/brew-message-modal.component';
import {ConfirmAction} from 'projects/brew-modal-lib/src/lib/action/confirm-action';
import {CancelAction} from 'projects/brew-modal-lib/src/lib/action/cancel-action';
import {BrewConfirmModalComponent} from 'projects/brew-modal-lib/src/lib/modal/confirm-modal/brew-confirm-modal.component';
import {BrewMessageModalBootstrapComponent} from 'projects/brew-modal-lib/src/lib/modal/message-modal/brew-message-modal-bootstrap.component';

@Component({
    selector: 'app-demo',
    template: `
        <brew-message-modal #modalMessage>
            <p>This is a message.</p>
        </brew-message-modal>
        <button (click)="toggleMessageModal()">Toggle Message Modal</button>
        <hr>
        <brew-message-modal-bootstrap #modalMessageBootstrap>
            <p>This is a message. In Bootstrap markup</p>
        </brew-message-modal-bootstrap>
        <button (click)="toggleMessageModalBootstrap()">Toggle Message Modal Bootstrap</button>
        <hr>
        <brew-confirm-modal #modalConfirm (confirmActionResult)="handleConfirm($event)">
            <p class="modal-text">Are you really, really sure?</p>
        </brew-confirm-modal>
        <hr>
        <button (click)="toggleConfirmModal()">Toggle Confirm Modal</button>
        <div *ngIf="accepted">
            <p>You're sure.</p>
        </div>
        <app-form-modal
                #modalForm
                title="Greeter"
                (confirmActionResult)="handleFormCompletion($event)"
                (cancelActionResult)="handleFormCancellation($event)"
        >
            <p class="modal-text">Who are you?</p>
        </app-form-modal>
        <hr>
        <button (click)="toggleFormModal()">Toggle Form Modal</button>
        <div *ngIf="message">
            <p>{{message}}</p>
        </div>
    `,
    styles: ['.modal-text{ margin: 0 0 1em; }']
})
export class DemoComponent implements OnInit, AfterViewInit {

    @ViewChild('modalMessage') modalMessage: BrewMessageModalComponent;
    @ViewChild('modalMessageBootstrap') modalMessageBootstrap: BrewMessageModalBootstrapComponent;
    @ViewChild('modalConfirm') modalConfirm: BrewConfirmModalComponent;
    @ViewChild('modalForm') modalForm: FormModalComponent;

    accepted: boolean;
    message: string;

    constructor() {
    }

    ngOnInit() {
    }

    ngAfterViewInit() {
        this.modalConfirm.setConfirmAction(new ConfirmAction());
        this.modalConfirm.setCancelAction(new CancelAction());
    }

    toggleMessageModal() {
        this.modalMessage.toggle();
    }

    toggleConfirmModal() {
        this.accepted = false;
        this.modalConfirm.toggle();
    }

    toggleFormModal() {
        this.message = '';
        this.modalForm.toggle();
    }

    handleFormCompletion(message: string) {
        this.message = message;
    }

    handleConfirm(confirmed: boolean) {
        this.accepted = confirmed;
    }

    handleFormCancellation(message: string) {
        this.message = message;
    }

    toggleMessageModalBootstrap() {
        this.modalMessageBootstrap.toggle();
    }
}
