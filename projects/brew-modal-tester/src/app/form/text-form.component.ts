import {Component, OnInit} from '@angular/core';
import {FormBase} from 'projects/brew-modal-lib/src/lib/abstract/form/form-base';
import {FormBuilder, Validators} from '@angular/forms';

@Component({
    selector: 'app-form',
    template: `
        <div>
            <form [formGroup]="form" (ngSubmit)="onSubmit()">
                <label for="name">Name:</label>
                <input id="name" type="text" formControlName="text">
                <button type="submit">Submit</button>
            </form>
        </div>
    `,
    styles: []
})
export class TextFormComponent extends FormBase implements OnInit {

    constructor(private fb: FormBuilder) {
        super();
    }

    ngOnInit() {
        this.form = this.fb.group({
            text: ['', [Validators.required]]
        });
    }

    onSubmit(): void {
        this.formData.emit(this.form.value);
        this.resetForm();
    }
}
