export class GreeterAction implements Action {
    formData;

    constructor(formData) {
        this.formData = formData;
    }

    execute(data?: any): any {
        return 'Hello ' + this.formData.text;
    }
}
