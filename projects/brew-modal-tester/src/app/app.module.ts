import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {DemoComponent} from './demo/demo.component';
import {TextFormComponent} from './form/text-form.component';
import {FormModalComponent} from './modals/form-modal/form-modal.component';
import {BrewMessageModalComponent} from 'projects/brew-modal-lib/src/lib/modal/message-modal/brew-message-modal.component';
import {BrewConfirmModalComponent} from 'projects/brew-modal-lib/src/lib/modal/confirm-modal/brew-confirm-modal.component';
import {ReactiveFormsModule} from '@angular/forms';
import {BrewMessageModalBootstrapComponent} from '../../../brew-modal-lib/src/lib/modal/message-modal/brew-message-modal-bootstrap.component';
import {BrewConfirmModalBootstrapComponent} from '../../../brew-modal-lib/src/lib/modal/confirm-modal/brew-confirm-modal-bootstrap.component';

@NgModule({
    declarations: [
        AppComponent,
        DemoComponent,
        TextFormComponent,
        FormModalComponent,
        BrewMessageModalComponent,
        BrewConfirmModalComponent,
        BrewMessageModalBootstrapComponent,
        BrewConfirmModalBootstrapComponent
    ],
    imports: [
        BrowserModule,
        ReactiveFormsModule,
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
