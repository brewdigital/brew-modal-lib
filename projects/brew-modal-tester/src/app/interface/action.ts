interface Action {
    execute(data?: object): any;
}
