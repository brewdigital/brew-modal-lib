import {Component} from '@angular/core';
import {ModalBase} from '../../abstract/modal/modal-base';

@Component({
    selector: 'brew-message-modal-bootstrap',
    template: `
        <div class="modal" tabindex="-1" role="dialog" *ngIf="show">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header" *ngIf="title">
                        <h5 class="modal-title">{{title}}</h5>
                        <button
                                (click)="onClose()"
                                type="button"
                                class="close"
                                aria-label="Close"
                        >
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <ng-content></ng-content>
                    </div>
                    <div class="modal-footer">
                        <button
                                (click)="onClose()"
                                type="button"
                                class="btn btn-secondary"
                        >Ok</button>
                    </div>
                </div>
            </div>
        </div>
    `,
})
export class BrewMessageModalBootstrapComponent extends ModalBase {}
