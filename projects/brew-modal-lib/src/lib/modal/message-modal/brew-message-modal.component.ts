import {Component} from '@angular/core';
import {ModalBase} from '../../abstract/modal/modal-base';

@Component({
    selector: 'brew-message-modal',
    template: `
        <div class="brew-modal-overlay brew-modal--message" *ngIf="show">
            <div class="brew-modal-holder">
                <div *ngIf="title" class="brew-modal-header brew-modal--padding brew-modal--section-divider">
                    <h2 class="brew-modal-title">{{title}}</h2>
                </div>
                <div class="brew-modal-body brew-modal--padding brew-modal--section-divider">
                    <ng-content></ng-content>
                </div>
                <div class="brew-modal-footer brew-modal--padding">
                    <button (click)="onClose()">Ok</button>
                </div>
            </div>
        </div>
    `,
})
export class BrewMessageModalComponent extends ModalBase {}
