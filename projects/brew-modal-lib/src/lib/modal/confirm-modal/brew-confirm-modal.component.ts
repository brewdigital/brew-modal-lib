import {Component} from '@angular/core';
import {ModalActionBase} from '../../abstract/modal/modal-action-base';

@Component({
    selector: 'brew-confirm-modal',
    template: `
        <div class="brew-modal-overlay brew-modal--confirm" *ngIf="show">
            <div class="brew-modal-holder">
                <div *ngIf="title" class="brew-modal-header brew-modal--padding brew-modal--section-divider">
                    <h2 class="brew-modal-title">{{title}}</h2>
                </div>
                <div class="brew-modal-body brew-modal--padding brew-modal--section-divider">
                    <ng-content></ng-content>
                </div>
                <div class="brew-modal-actions brew-modal--padding">
                    <button (click)="confirmActionAndClose()" class="button button--confirm">Ok</button>
                    <button (click)="cancelActionAndClose()" class="button button--decline">Cancel</button>
                </div>
            </div>
        </div>
    `,
})
export class BrewConfirmModalComponent extends ModalActionBase {}
