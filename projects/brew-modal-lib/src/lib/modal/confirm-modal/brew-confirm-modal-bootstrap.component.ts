import {Component} from '@angular/core';
import {ModalActionBase} from '../../abstract/modal/modal-action-base';

@Component({
    selector: 'brew-confirm-modal-bootstrap',
    template: `
        <div class="modal" tabindex="-1" role="dialog" *ngIf="show">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header" *ngIf="title">
                        <h5 class="modal-title">{{title}}</h5>
                        <button
                            (click)="onClose()"
                            type="button"
                            class="close"
                            aria-label="Close"
                        >
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <ng-content></ng-content>
                    </div>
                    <div class="modal-footer">
                        <button
                            (click)="confirmActionAndClose()"
                            type="button"
                            class="btn btn-primary"
                        >Ok</button>
                        <button
                            (click)="cancelActionAndClose()"
                            type="button"
                            class="btn btn-secondary"
                        >Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    `,
})
export class BrewConfirmModalBootstrapComponent extends ModalActionBase {}
