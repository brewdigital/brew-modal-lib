import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BrewConfirmModalComponent} from './modal/confirm-modal/brew-confirm-modal.component';
import {BrewMessageModalComponent} from './modal/message-modal/brew-message-modal.component';
import {BrewConfirmModalBootstrapComponent} from './modal/confirm-modal/brew-confirm-modal-bootstrap.component';
import {BrewMessageModalBootstrapComponent} from './modal/message-modal/brew-message-modal-bootstrap.component';

@NgModule({
    declarations: [
        BrewConfirmModalComponent,
        BrewMessageModalComponent,
        BrewConfirmModalBootstrapComponent,
        BrewMessageModalBootstrapComponent
    ],
    imports: [
        CommonModule
    ],
    exports: [
        BrewConfirmModalComponent,
        BrewMessageModalComponent,
        BrewConfirmModalBootstrapComponent,
        BrewMessageModalBootstrapComponent
    ]
})
export class BrewModalModule {
}
