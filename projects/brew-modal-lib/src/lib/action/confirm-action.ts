import {Action} from '../interface/action';

export class ConfirmAction implements Action {
    execute(data?: any): any {
        return true;
    }
}
