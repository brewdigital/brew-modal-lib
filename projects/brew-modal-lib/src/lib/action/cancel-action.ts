import {Action} from '../interface/action';

export class CancelAction implements Action {
    execute(data?: any): any {
        return false;
    }
}
