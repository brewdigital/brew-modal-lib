import {FormGroup} from '@angular/forms';
import {EventEmitter, Output} from '@angular/core';

export abstract class FormBase {

    @Output() formData = new EventEmitter();

    form: FormGroup;

    abstract onSubmit(): void;

    resetForm() {
    }
}
