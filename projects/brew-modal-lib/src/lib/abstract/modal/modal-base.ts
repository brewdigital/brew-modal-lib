import {Input} from '@angular/core';

export abstract class ModalBase {
    @Input()
    show = false;

    @Input()
    title: any;

    showModal() {
        this.show = true;
    }

    hideModal() {
        this.show = false;
    }

    toggle() {
        this.show ? this.hideModal() : this.showModal();
    }

    onClose() {
        this.hideModal();
    }
}
