import {EventEmitter, Output} from '@angular/core';
import {ModalBase} from './modal-base';
import {Action} from '../../interface/action';

export abstract class ModalActionBase extends ModalBase {
    @Output() confirmActionResult = new EventEmitter();
    @Output() cancelActionResult = new EventEmitter();
    protected confirmAction: Action;
    protected cancelAction: Action;

    setConfirmAction(onConfirmCallback: Action) {
        this.confirmAction = onConfirmCallback;
    }

    setCancelAction(onCancelCallback: Action) {
        this.cancelAction = onCancelCallback;
    }

    confirmActionAndClose() {
        if (this.confirmAction) {
            this.confirmActionResult.emit(this.confirmAction.execute());
        }
        this.hideModal();
    }

    cancelActionAndClose() {
        if (this.cancelAction) {
            this.cancelActionResult.emit(this.cancelAction.execute());
        }
        this.hideModal();
    }
}
